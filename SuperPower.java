import javax.swing.JOptionPane;

/**
* SuperPower class
*
* display couple of graphical panes
*
* at first, prompt user for input
*
* at second, print output all in uppercase
*
* compile example ----- javac SuperPower.java
*
* run example ------ java SuperPower
*
*/
class SuperPower{
    public static void main(String[] args){
        String power=JOptionPane.showInputDialog("What is your super power?");
        power=power.toUpperCase();
        JOptionPane.showMessageDialog(null,power+" TO THE RESCUE!");
    }
}
