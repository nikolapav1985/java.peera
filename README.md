Programming assignment 1
------------------------

Implement a Java program and couple of graphical panes. First pane prompts user for input, second pane prints text all upper case.

Details
-------

- SuperPower.java (check comments for details, compile and run instructions, all files can be opened using simple editors like Notepad, Notepad++ and similar)

Test environment
----------------

OS lubuntu 16.04 javac version 1.8.0
